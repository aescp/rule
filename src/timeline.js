const state = () => ({
    multi: false,
    timeline_skeleton: {
        plays: 1,
        hits: 0,
        last_number: -1,
        tops: [-1, -1, -1],
        game_id: -1,
        timelines: [1]
    },
    timelines: [],
})

const mutations = {
    toggleMultiple (state) {
        state.multi = !state.multi
    },
    addTimeline (state, game_index) {
        state.timeline_skeleton.game_id = game_index
        state.timelines.push(state.timeline_skeleton)
    },
    ADD_TOPS (state, data) {
        console.log('set top ' + data.tops)
        state.timelines.find(timeline => timeline.game_id === data.game_id).tops = data.tops
    },
    playTimelines (state, data) {
        let timeline = state.timelines.find(timeline => timeline.game_id === data.game_id)
        const tops = timeline.tops
        const number = "" + data.number
        timeline.last_number = number
        console.log('play with top ' + number)

        let hit = false
        // multi or single game
        if (state.multi) {
            if (tops.indexOf(number) !== -1)
                hit = true
        } else {
            if (tops[0] === number)
                hit = true
        }
        // number hit
        if (hit) {
            timeline.timelines.push(1)
            timeline.plays = 1
            timeline.hits += 1
        } else {
            timeline.plays += 1
            timeline.timelines[timeline.hits] = timeline.plays
        }
        // reset
        let find = state.timelines.find(timeline => timeline.game_id === data.game_id)
        find = timeline
    }
}

const actions = {
    addTops ({ commit }, data) {
        return new Promise((resolve) => {
            commit('ADD_TOPS', data)
            resolve()
        })
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}