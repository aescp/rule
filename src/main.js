import Vue from 'vue';
import App from './App.vue';
import Vuex from 'vuex'
import timeline from "./timeline"

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        timeline: timeline
    },
    state: {
        numbers: [],
        planets: [],
        last: -1,
        marked: [],
        list: {},
    },
    mutations: {
        play (state, number) {
            state.numbers.push(number)
            state.last = number
            console.log('set number as ' + number)
            state.marked = state.marked.map((res) => res + 1)
        },
        planets (state, planets) {
            state.planets.push(planets);
        },
        list (state, list) {
            state.list = list
        },
        mark (state, gameIndex) {
            const index = state.marked.indexOf(gameIndex);
            if (index > -1) {
                state.marked.splice(index, 1);
            } else {
                state.marked.push(gameIndex)
            }
        }
    }
})

Vue.mixin({
    methods: {
        getColor (number) {
            number = parseInt(number)

            const red = [1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36]
            const black = [2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35]

            if (red.includes(number)) {
                return 'red'
            } else if (black.includes(number)) {
                return 'white'
            }

            return 'green'
        },
        until (number) {
            while (number > 36)
                number -= 37
            return number
        },
        findTimelineById (timelines, game_id) {
            return timelines.find(timeline => timeline.game_id === game_id)
        }
    },
})

new Vue({
    el: '#app',
    render: h => h(App),
    store: store
});